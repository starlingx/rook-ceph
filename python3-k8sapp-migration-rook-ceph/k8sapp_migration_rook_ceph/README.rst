k8sapp_migration_rook_ceph
================

This project contains StarlingX Kubernetes application specific python plugins
for the rook ceph application. These plugins are required to
integrate the application into the StarlingX application framework and to
support the various StarlingX deployments.
